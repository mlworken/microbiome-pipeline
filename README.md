[![Snakemake](https://img.shields.io/badge/snakemake-≥3.4.1-brightgreen.svg?style=flat-square)](https://bitbucket.org/johanneskoester/snakemake)

# Vet Med Microbiome Pipeline

This is a Snakemake pipeline designed to process 16S gene survey data using the [UPARSE](http://drive5.com/uparse/) OTU clustering method.  Using QIIME it also assigns taxonomy to the representative OTUs with the RDP classifer, aligns the sequences with PyNAST and constructs a tree with FastTree.  The pipeline has been developed for the Faculty of [Veterinary Medicine](http://www.vet.ucalgary.ca) at the University of Calgary.

This repository is provided for reference purposes for publications that use this pipeline and is not provided as a tool for others to use.  This means there is no support or help provided.  That being said anyone is welcome to clone the repository and use the pipeline or feel free to use it as a guide to write your own Snakemake pipeline.

## Install

Clone this repository to a location of your choosing.  That's it.  Type `./vmmp --help` to see if it works.  Symlink the python vmmp script to a directory in your path for convenience (i.e. `ln -s vmmp ~/bin`).

Highly recommend using virtualenv or a [conda](https://anaconda.org) virtual environment to manage your install and associated dependencies.  See the snakemake [webpage](https://bitbucket.org/johanneskoester/snakemake/wiki/Home) for details on how to do this with your snakemake install.


## Dependencies

Check dependencies with `vmmp --depends`

As configured the snakefile will load the required dependencies using environment modules installed on our local server.  As long as the dependencies below are in your path then there is no need to use the modules.  Simply comment those lines out.  Also, you'll need to comment out the `onsuccess` and `onerror` portions or replace with your own code.  The `push` command is custom script to push a notification to my [Pushover](https://pushover.net) account.

A note on using QIIME: Snakemake runs in Python 3 and QIIME in Python 2.  To get around this the pipeline assumes you have installed QIIME in it's own anaconda virtual environment named 'qiime'.  Prior to running any QIIME commands the pipeline will load this environment.

- Python: 3 and above
- [qiime](http://qiime.org): 1.9.1
- [seqtk](https://github.com/lh3/seqtk): 1.0
- [Snakemake](https://bitbucket.org/johanneskoester/snakemake): 3.4.1
- [blast](http://www.ncbi.nlm.nih.gov/books/NBK279690/): 2.2.25
- [vsearch](https://github.com/torognes/vsearch): 1.1.3
- [usearch](http://www.drive5.com/usearch/download.html): 8.0.1623
- UPARSE python [scripts](http://drive5.com/python/)
- [cutadapt](http://cutadapt.readthedocs.org/en/stable/): 1.8.3
- R: 3.2.2 with the following packages installed: ShortRead, dplyr, ggplot2, stringr, tidyr, readr

## Config file

The pipeline requires a config file, written in yaml, to run.  See the provided example file. Most options are self-explanatory and simple to setup.  Example primer sets are given for common protocols at our institution - these can be changed as required.  Sample names should be unique and contained within the file name.

## Quality check

As of now the pipeline requires manual inspection of the quality data to determine the best parameters for quality filtering.  This is done by filtering a single sample with a range of different parameters and inspecting the results to determine the optimal setting for the expected error (-fastq_maxee) and truncation length (-fastq_trunclen) parameters provided to the usearch -fastq_filter command.  

The quality check is run with `vmmp --quality_test CONFIGFILE` where you have specified in the config file which sample you'd like to use for checking.  Results will be in the qual_test folder with two pdf files, one showing the percentage of sequences remaining after filtering with various parameters and the other a histogram of sequence lengths.  Use these plots to help you decide what to set for these two parameters in the config file.

## Running

Once the quality filtering parameters have been determined and the config file constructed the pipeline can be tested with `vmmp --dryrun CONFIGFILE`.  This will test the snakemake workflow and show the steps to be run.  The actual run is started with `vmmp --cores [number of cores to use] CONFIGFILE`.

Note that if you prefer not to use the vmmp python script interface you can of course run snakemake directly with the provided snakefile.  All output will be the same.


## Results

There are various intermediate folders including a folder with log files that can be inspected if an error is encountered.  The main output is in the 'results' folder and contains the otu table with taxonomy in biom format.  A fasta file with the representative OTU sequences and the phylogenetic tree are also present.  


## Pipeline summary

Most of the preprocessing steps for creating the OTU table are as outlined on the UPARSE [webpage](http://drive5.com/usearch/manual/uparse_pipeline.html).  A detailed flow chart is shown in vmmp.png and the basic steps are as follows.

1. Clipping the forward and reverse 16S primers, and any adaptor contamination, with cutadapt
2. Merge the forward and reverse reads with usearch
3. Filter with expected error method and truncate sequences at fixed length
4. Add sample label to read names and combine all sequence files
5. Dereplicate and remove singletons with vsearch
6. Cluster with `usearch -cluster_otus`
7. Remove any remaining chimeras with `usearch -uchime_ref`
8. Map reads to OTUs with `vsearch -usearch_global`
9. Assign taxonomy with RDP classifer in Qiime, using the Greengenes database (Qiime default)
10. Align OTUs with PyNast and filter to remove gaps, both in Qiime
11. Build tree with FastTree in Qiime
12. Make a biom file with OTU table and taxonomy added

## Future development

This pipeline will evolve as the analysis tools for 16S data evolve.  One major goal is to remove the QIIME dependency.  New tools and features will be developed in a separate branch, with master remaining stable.  
