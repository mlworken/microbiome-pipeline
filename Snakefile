# *******************************
# * Vet med microbiome pipeline *
# *******************************

# **** Modules ****

# if these are in the path already then comment this section out
exec(open("/usr/share/Modules/init/python3.py").read())

module('load', 'applications/usearch/8.0.1623')
module('load', 'applications/vsearch/1.1.3')
module('load', 'applications/seqtk/1.0')
module('load', 'applications/usearch/8.0.1623')
module('load', 'applications/blast/2.2.25')



# **** Variables ****

#configfile: "config.yaml"


# **** Imports ****

import glob
import datetime
import os
from snakemake.utils import R
import random


# ***** Finshing and Errors *****

onsuccess:
    shell("push 'VMMP: {config[run_name]}' 'Finished'")

onerror:
    shell("push 'VMMP: {config[run_name]}' 'Stopped with error.'")




# **** Rules ****

rule all:
    input: "results/otus.tre", "results/otu_table_rdp.biom", "results/otu_table_summary.txt"

rule clip_primers:
    input: r1 = lambda wildcards: glob.glob("{directory}/{sample}_*R1*.fastq*".format(directory=config["read_directory"], sample=wildcards.sample)),
           r2 = lambda wildcards: glob.glob("{directory}/{sample}_*R2*.fastq*".format(directory=config["read_directory"], sample=wildcards.sample))
    output: r1="clipped/{sample}_R1.cut", r2="clipped/{sample}_R2.cut"
    log: "logs/{sample}.log"
    message: "Clipping with read files:\nR1: {input.r1}\nR2: {input.r2}"
    shell: "cutadapt -e 0 -O 17 -g {config[fwd_primer]} -G {config[rev_primer]} -a {config[rev_primer_rc]} -A {config[fwd_primer_rc]} -o {output.r1} -p {output.r2} {input.r1} {input.r2} >> {log}"

rule merge_pairs:
    input: r1=rules.clip_primers.output.r1, r2=rules.clip_primers.output.r2
    output: "merged/{sample}.merged"
    log: "logs/{sample}.log"
    message: "Merging read pairs: {input.r1} and {input.r2}"
    shell: "usearch -fastq_mergepairs {input.r1} -reverse {input.r2} -fastqout {output} -threads 1 &>> {log}"

rule calc_stats:
    input: rules.merge_pairs.output
    output: "stats/{sample}.stats"
    message: "Calculating sequence quality statistics"
    shell: "usearch -fastq_eestats {input} -output {output} -threads 1"

rule quality_test:
    input: "merged/{choice}.merged".format(choice=config["sample_for_quality"])
    output: "qual_test/seq_len_plot.pdf", "qual_test/quality_test_plot.pdf"
    threads: 30
    log: "logs/quality_test.log"
    run:
        avg_len = shell("awk 'NR%4==2{{sum+=length($0)}}END{{print sum/(NR/4)}}' {input}", iterable=True)
        avg_len = int(float(list(avg_len)[0]))
        cutoffs = range(100, avg_len + 10, 10)
        cutoffs = [str(x) for x in cutoffs]
        cutoffs = " ".join(cutoffs)
        with open(log[0], 'w') as lf:
            lf.write("Using {} for test\n".format(input))
            lf.write("Determined average read length to be {}\n".format(avg_len))
            lf.write("Using read length cutoff values of: {}\n".format(cutoffs))

        shell("parallel --jobs {threads} 'usearch -fastq_filter {input} -fastaout qual_test/qtest_EE{{1}}_trunc{{2}}.fasta -fastq_maxee {{1}} -fastq_trunclen {{2}} -threads 1' ::: 0.25 0.5 1 1.5 2 3 ::: {cutoffs} ")

        path = workflow.basedir
        qual_script = os.path.join(path, 'quality_test.R')
        shell("Rscript {qual_script} qual_test {input}")

rule filter_seqs:
    input: merged=rules.merge_pairs.output
    output: "filtered/{sample}.filtered"
    log: "logs/{sample}.log"
    message: "Quality filtering {input.merged}"
    shell: "usearch -fastq_filter {input.merged} -fastaout {output} -fastq_maxee {config[max_expected_error]} -fastq_trunclen {config[read_length_cutoff]} -threads 1 &>> {log}"

rule add_barcode_label:
    input: rules.filter_seqs.output
    output: "with_barcode/{sample}.renamed"
    params: name="barcodelabel={sample};"
    message: "Adding sample information to read name for {input}"
    shell: 'seqtk rename {input} "{params.name}" >{output}'

rule combine_all_seqs:
    input: expand("with_barcode/{sample}.renamed", sample=config["samples"])
    output: "cluster/all_seqs.fasta"
    message: "Concatenating all sequence files to {output}"
    shell: 'cat {input} > {output}'

rule dereplicate:
    input: rules.combine_all_seqs.output
    output: "cluster/derep.fasta"
    threads: config["num_threads"]
    message: "Dereplicating {input} with {threads} threads."
    shell: "vsearch --threads {threads} --derep_fulllength {input} --output {output} --sizeout"

rule remove_singletons:
    input: rules.dereplicate.output
    output: "cluster/derep_sorted.fasta"
    message: "Sorting and removing singletons from {input}"
    shell: "vsearch --threads {threads} --sortbysize {input} --output {output} --minsize 2"

rule cluster:
    input: rules.remove_singletons.output
    output: "cluster/clustered.fasta"
    log: "logs/cluster.log"
    benchmark: "benchmarks/cluster.json"
    message: "Now clustering {input} with usearch.  Output is in {output} and log is in {log}."
    shell: "usearch -cluster_otus {input} -otus {output} -uparseout cluster/uparse_out.txt -relabel OTU_ -sizein -sizeout &> {log}"

rule remove_chimeras:
    input: rules.cluster.output
    output: "cluster/otus_nochime.fasta"
    log: "logs/chimeras.log"
    message: "Remvoing remaining chimeras from {input} using uchime"
    shell: "usearch -uchime_ref {input} -db {config[chimera_db]} -uchimeout cluster/uchime_results.txt -nonchimeras {output} -strand plus &> {log}"

rule rename_otus:
    input: rules.remove_chimeras.output
    output: "results/otus.fasta"
    message: "Renaming OTUs to something a little nicer"
    shell: "seqtk rename {input} OTU_ > {output}"

rule map_reads:
    input: seqs=rules.combine_all_seqs.output, db=rules.rename_otus.output
    output: "cluster/mapped_reads.uc"
    threads: config["num_threads"]
    message: "Mapping reads in {input.seqs} back to otus in {input.db} with vsearch using {threads} threads"
    benchmark: "benchmarks/map_reads.json"
    shell: "vsearch --threads {threads} -usearch_global {input.seqs} -db {input.db} -strand plus -id 0.97 -uc {output} -maxaccepts 8 -maxrejects 64 -top_hits_only"

rule convert_uc:
    input: rules.map_reads.output
    output: "cluster/otu_table.txt"
    message: "Creatiung OTU table from {input}"
    shell: "source activate qiime; python d5_python_scripts/uc2otutab.py {input} > {output}"

rule assign_taxonomy:
    input: rules.rename_otus.output
    output: "rdp_taxonomy/otus_tax_assignments.txt"
    params: dir="rdp_taxonomy"
    threads: config["num_threads"]
    message: "Assigning taxonomy with the RDP classifier, using {threads} thread(s).  Output is in {output}."
    benchmark: "benchmarks/assign_taxonomy.json"
    shell:  "source activate qiime; parallel_assign_taxonomy_rdp.py -i {input} --jobs_to_start={threads} -o {params.dir} --rdp_classifier_fp {config[rdp_jar_path]}"

rule align_otus:
    input: rules.rename_otus.output
    output: "aligned/otus_aligned.fasta"
    params: dir="aligned"
    threads: config["num_threads"]
    message: "Aligning sequences in {output} with pynast, using {threads} thread(s). Output is in {output}"
    benchmark: "benchmarks/align.json"
    shell: "source activate qiime; parallel_align_seqs_pynast.py -i {input} -o {params.dir} -O {threads}"

rule filter_alignment:
    input: rules.align_otus.output
    output: "filtered_aligned/otus_aligned_pfiltered.fasta"
    params: dir="filtered_aligned"
    message: "Filtering alignment for gaps. Output is in {output}"
    shell: "source activate qiime; filter_alignment.py -i {input} -o {params.dir}"

rule make_tree:
    input: rules.filter_alignment.output
    output: "results/otus.tre"
    message: "Building phylogeny - tree is saved in {output}"
    shell: "source activate qiime; make_phylogeny.py -i {input} -o {output} -r midpoint"

rule create_biom:
    input: rules.convert_uc.output
    output: "otus/otu_table.biom"
    message: "Converting text OTU table into a biom file ({output})"
    shell: "source activate qiime; [ -f {output} ] && rm -f {output}; biom convert --table-type='OTU table' -i {input} -o {output} --to-hdf5"

rule add_taxonomy:
    input: table=rules.create_biom.output, taxonomy=rules.assign_taxonomy.output
    output: "otus/otu_table_rdp.biom"
    message: "Adding taxonomy from {input.taxonomy} to {input.table}.  Output is saved in {output}"
    shell: "source activate qiime; [ -f {output} ] && rm -f {output}; biom add-metadata --sc-separated=taxonomy --observation-header=OTUID,taxonomy --observation-metadata-fp={input.taxonomy}  -i {input.table} -o {output}"

rule summarize_table:
    input: rules.add_taxonomy.output
    output: "results/otu_table_summary.txt"
    message: "Creating summary: {output}"
    shell: "source activate qiime; [ -f {output} ] && rm -f {output}; biom summarize-table -i {input} -o {output}"

rule convert_table:
    input: rules.add_taxonomy.output
    output: "results/otu_table_rdp.biom"
    message: "Converting biom otu table to json format so it can loaded into R.  Output is saved to {output}"
    shell: "source activate qiime; [ -f {output} ] && rm -f {output}; biom convert -i {input} -o {output} --table-type='OTU table' --to-json"

rule preliminary_analysis:
    input: rules.convert_table.output
    output: "results/preliminary_analysis.html"
    message: "Doing a preliminary analysis"
    run:
        shell("mv preliminary_analysis.Rmd results/")
        R("""rmarkdown::render('results/preliminary_analyis.Rmd', 'all')""")


